package ru.shirykalov.anatoly.classiconline.data.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CommentRequest {

    @SerializedName("approved")
    private List<Long> approved = new ArrayList<>();

    @SerializedName("declined")
    private List<Long> declined = new ArrayList<>();

    public CommentRequest(Long commentId, boolean isApproved) {
        if (isApproved) {
            approved.add(commentId);
        } else {
            declined.add(commentId);
        }
    }

    public CommentRequest(List<Long> approved, List<Long> declined) {
        this.approved = approved;
        this.declined = declined;
    }
}
