package ru.shirykalov.anatoly.classiconline.data.repository.favorites;

import ru.shirykalov.anatoly.classiconline.data.model.Directory;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.data.model.response.Response;
import ru.shirykalov.anatoly.classiconline.data.model.result.DirectoriesResult;
import ru.shirykalov.anatoly.classiconline.data.model.result.PerformsResult;

public interface FavoritesRepository {

    boolean setFavoriteFilterId(int id);

    int getFavoriteFilterId();


    void like(int performId,
              Response.Start startListener,
              Response.Success<Void> successListener,
              Response.Error errorListener,
              Response.Complete completeListener);

    void perform(int performId,
                 Response.Start startListener,
                 Response.Success<Perform> successListener,
                 Response.Error errorListener,
                 Response.Complete completeListener);

    void performs(int offset,
                  int limit,
                  Response.Start startListener,
                  Response.Success<PerformsResult> successListener,
                  Response.Error errorListener,
                  Response.Complete completeListener);

    void directories(int offset,
                     Response.Start startListener,
                     Response.Success<DirectoriesResult> successListener,
                     Response.Error errorListener,
                     Response.Complete completeListener);

    void createDirectory(String name,
                         Response.Start startListener,
                         Response.Success<Directory> successListener,
                         Response.Exist existListener,
                         Response.Error errorListener,
                         Response.Complete completeListener);

    void addPerformToDirectory(Directory directory,
                               Perform perform,
                               Response.Start startListener,
                               Response.Success<Void> successListener,
                               Response.Exist existListener,
                               Response.Error errorListener,
                               Response.Complete completeListener);

    void removePerformFromDirectory(Directory directory,
                                    Perform perform,
                                    Response.Start startListener,
                                    Response.Success<Boolean> successListener,
                                    Response.Error errorListener,
                                    Response.Complete completeListener);

}
