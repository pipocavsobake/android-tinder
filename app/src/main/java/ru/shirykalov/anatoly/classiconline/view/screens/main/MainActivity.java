package ru.shirykalov.anatoly.classiconline.view.screens.main;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.data.repository.comments.CommentsRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.comments.CommentsRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.player.PlayerService;
import ru.shirykalov.anatoly.classiconline.player.PlayerStateListener;
import ru.shirykalov.anatoly.classiconline.player.PlayerViewListener;
import ru.shirykalov.anatoly.classiconline.utils.Utils;
import ru.shirykalov.anatoly.classiconline.view.custom.PlaybackFullscreenView;
import ru.shirykalov.anatoly.classiconline.view.custom.PlaybackView;
import ru.shirykalov.anatoly.classiconline.view.screens.comments.CommentsFragment;
import ru.shirykalov.anatoly.classiconline.view.screens.favorites.FavoritesFragment;
import ru.shirykalov.anatoly.classiconline.view.screens.login.ClonLoginActivity;
import ru.shirykalov.anatoly.classiconline.view.screens.login.LoginActivity;
import ru.shirykalov.anatoly.classiconline.view.screens.recent.RecentAudioFragment;
import ru.shirykalov.anatoly.classiconline.view.screens.search.SearchAudioFragment;
import ru.shirykalov.anatoly.classiconline.view.screens.settings.SettingsActivity;

import static android.support.design.widget.BottomSheetBehavior.BottomSheetCallback;
import static android.support.design.widget.BottomSheetBehavior.STATE_COLLAPSED;
import static android.support.design.widget.BottomSheetBehavior.from;
import static android.support.v4.view.GravityCompat.START;
import static ru.shirykalov.anatoly.classiconline.R.layout.activity_main;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.playerPanelView)
    RelativeLayout bottomSheetPlayer;

    @BindView(R.id.playbackView)
    PlaybackView playbackView;

    @BindView(R.id.playbackViewFullscreen)
    PlaybackFullscreenView playbackViewFullscreen;

    private AuthRepository authRepository;
    private CommentsRepository commentsRepository;

    private Disposable disposable;
    private Disposable timer;

    private PlayerService playerService;

    private boolean isStartedMusic = false;

    private PlayerStateListener playerStateListener = new PlayerStateListener() {

        @Override
        public void onInit(Perform perform) {
            from(bottomSheetPlayer).setPeekHeight(playbackView.getHeight());
            playbackView.setPerform(perform, playerService.getResumePosition(), playerService.getDuration());
            playbackViewFullscreen.setPerform(perform, playerService.getResumePosition(), playerService.getDuration());
        }

        @Override
        public void onPlay() {
            startProgressUpdates();
            playbackView.play();
            playbackViewFullscreen.play();
        }

        @Override
        public void onPause() {
            if (timer != null) timer.dispose();
            playbackView.pause();
            playbackViewFullscreen.pause();
        }

        @Override
        public void onStop() {
            if (timer != null) timer.dispose();
            from(bottomSheetPlayer).setPeekHeight(0);
        }

        @Override
        public void onFavorite() {
            playbackView.setFavorite();
            playbackViewFullscreen.setFavorite();
        }

        @Override
        public void onComplete() {
            pushService(PlayerService.actionStop(MainActivity.this));
        }
    };

    private PlayerViewListener playbackListener = new PlayerViewListener() {

        @Override
        public void onPlayPauseClick() {
            pushService(PlayerService.actionPlayPause(MainActivity.this));
        }

        @Override
        public void onFavoriteClick(Perform perform) {
            if (!isStartedMusic) return;
            pushService(PlayerService.actionFavorite(MainActivity.this, perform));
        }

        @Override
        public void startSeek() {
            timer.dispose();
        }

        @Override
        public void endSeek(int progress) {
            playerService.seekTo(progress);
        }

        @Override
        public void onCloseFullscreenPlayer() {
            from(bottomSheetPlayer).setPeekHeight(playbackView.getHeight());
            from(bottomSheetPlayer).setState(STATE_COLLAPSED);
        }

        @Override
        public void onStopClick() {
            pushService(PlayerService.actionStop(MainActivity.this));
        }
    };

    private ServiceConnection musicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            playerService = ((PlayerService.PlayerBinder) service).getService();
            playerService.setPlayerListener(playerStateListener);
            playerService.initMediaSession();

            if (playerService.getActiveAudio() != null) {
                playbackView.setPerform(playerService.getActiveAudio(), playerService.getResumePosition(), playerService.getDuration());
                playbackViewFullscreen.setPerform(playerService.getActiveAudio(), playerService.getResumePosition(), playerService.getDuration());
                playbackView.setStatus(playerService.getPlaybackStatus());
                playbackViewFullscreen.setStatus(playerService.getPlaybackStatus());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };


    //==============================================================================================
    // Android
    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);
        ButterKnife.bind(this);
        authRepository = new AuthRepositoryImpl(this);
        commentsRepository = new CommentsRepositoryImpl(this);
        setupDrawer();
        setupUI();
        startConnectivityListener();
        bindService(new Intent(this, PlayerService.class), musicConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 100 && isStartedMusic) {
            pushService(PlayerService.actionStop(this));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAuth();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) timer.dispose();
        if (disposable != null) disposable.dispose();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(START)) {
            drawer.closeDrawer(START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_audio_search:
                checkClonAuth();
                replaceFragment(new SearchAudioFragment());
                break;
            case R.id.nav_audio_recent:
                checkClonAuth();
                replaceFragment(new RecentAudioFragment());
                break;
            case R.id.nav_comments:
                checkClonAuth();
                replaceFragment(new CommentsFragment());
                break;
            case R.id.nav_favorite:
                checkClonAuth();
                replaceFragment(new FavoritesFragment());
                break;
            case R.id.nav_settings:
                SettingsActivity.start(this);
                return false;
            case R.id.nav_logout:
                logOut();
                break;
            default:
                ((FrameLayout) findViewById(R.id.container)).removeAllViews();
                break;
        }

        drawer.closeDrawer(START);
        from(bottomSheetPlayer).setState(STATE_COLLAPSED);
        return true;
    }


    //==============================================================================================
    // Public
    //==============================================================================================

    public void startPlayer(Perform perform, boolean isContinue) {
        if (playerService == null) {
            startMusicService(perform, isContinue);
        } else {
            playerService.setActiveAudio(perform, isContinue);
        }
    }

    public void likePerform(Perform perform) {
        playbackListener.onFavoriteClick(perform);
    }

    public void logOut() {
        authRepository.resetAuthorized();
        LoginActivity.start(this);
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void setupDrawer() {
        setSupportActionBar(toolbar);

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupUI() {
        playbackView.setListener(playbackListener);
        playbackViewFullscreen.setListener(playbackListener);
        initBottom();
    }


    private void initBottom() {
        from(bottomSheetPlayer).setPeekHeight(0);
        from(bottomSheetPlayer).setBottomSheetCallback(new BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                playbackViewFullscreen.setAlpha(slideOffset);
                playbackView.setAlpha(1 - slideOffset);
            }

        });
    }

    private void checkAuth() {
        if (authRepository.isAuthorized()) LoginActivity.start(this);
    }

    private void checkClonAuth() {
        if (authRepository.isNotClonAuthorized()) ClonLoginActivity.start(this);
    }

    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    private void sendUnsentComments() {
        commentsRepository.sendUnsentComments();
    }

    private void startProgressUpdates() {
        timer = Utils.updatesPlayerTime(t -> runOnUiThread(() -> {
            playbackView.setCurrentPosition(playerService.getResumePosition());
            playbackViewFullscreen.setCurrentPosition(playerService.getResumePosition());
        }));
    }

    private void startConnectivityListener() {
        disposable = Utils.connectivityListener(this, connectivity -> sendUnsentComments());
    }

    private void startMusicService(Perform perform, boolean isContinue) {
        isStartedMusic = true;
        pushService(PlayerService.startPlayerService(this, perform, isContinue));
    }

    private void pushService(Intent intent) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            startService(intent);
        } else {
            startForegroundService(intent);
        }
    }
}
