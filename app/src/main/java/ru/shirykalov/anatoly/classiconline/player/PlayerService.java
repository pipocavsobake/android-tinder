package ru.shirykalov.anatoly.classiconline.player;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.media.session.MediaSessionCompat;

import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.data.model.request.EventRequest;
import ru.shirykalov.anatoly.classiconline.data.repository.favorites.FavoritesRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.favorites.FavoritesRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.data.repository.statistic.StatisticRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.statistic.StatisticRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.utils.Utils;

import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.FAVORITE;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.INIT;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.PAUSE;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.PLAY;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.PLAY_PAUSE;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.STOP;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.valueOf;

public class PlayerService extends Service implements PlayerStatisticsListener {

    private final IBinder iBinder = new PlayerBinder();

    private StatisticRepository statisticRepository;
    private FavoritesRepository favoritesRepository;

    private PlaybackManager playbackManager;
    private PlayerNotificationManager notificationManager;
    private Perform activeAudio;
    private PlayerStateListener playerStateListener;

    private MediaSessionCompat.Callback mediaSessionCallback = new MediaSessionCompat.Callback() {

        @Override
        public void onPlay() {
            play();
            prepareEventPlay();
        }

        @Override
        public void onPause() {
            pause();
            prepareEventPause();
        }

        @Override
        public void onStop() {
            playbackManager.release();
            stopSelf();
            playerStateListener.onStop();
            prepareEventStop();
        }

        @Override
        public void onSeekTo(long position) {

        }

        @Override
        public void onCustomAction(String action, Bundle extras) {
            switch (valueOf(action.toUpperCase())) {
                case PLAY_PAUSE:
                    notificationManager.handleIncomingActions(new Intent(isPlaying() ? PAUSE.name() : PLAY.name()));
                    break;
                case FAVORITE:
                    likePerform();
                    break;
                case BROWSER:
                    Utils.openBrowser(PlayerService.this, activeAudio);
                    break;
            }
        }
    };

    public static Intent startPlayerService(Context context, Perform perform, boolean isContinue) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.setAction(INIT.name());
        intent.putExtra("perform", perform);
        intent.putExtra("isContinue", isContinue);
        return intent;
    }

    public static Intent actionPlayPause(Context context) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.setAction(PLAY_PAUSE.name());
        return intent;
    }

    public static Intent actionFavorite(Context context, Perform perform) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.setAction(FAVORITE.name());
        intent.putExtra("perform", perform);
        return intent;
    }

    public static Intent actionStop(Context context) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.setAction(STOP.name());
        return intent;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        statisticRepository = new StatisticRepositoryImpl(this);
        favoritesRepository = new FavoritesRepositoryImpl(this);
        notificationManager = new PlayerNotificationManager(this);
        playbackManager = new PlaybackManager(getApplicationContext(), this);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra("perform")) {
            activeAudio = (Perform) intent.getSerializableExtra("perform");
            setActiveAudio(activeAudio, intent.getBooleanExtra("isContinue", false));
        }

        notificationManager.handleIncomingActions(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        playbackManager.release();
        notificationManager.release();
    }

    @Override
    public void onError() {
        prepareEventUnexpectedStop();
    }

    @Override
    public void onCompletion() {
        prepareEventFinish();
    }

    public int getResumePosition() {
        return playbackManager.getPosition();
    }

    public int getDuration() {
        return playbackManager.getDuration();
    }

    private void initPlayer(boolean isContinue) {
        playbackManager.reset(activeAudio, isContinue);
    }

    public void play() {
        playbackManager.resume();
    }

    public void pause() {
        playbackManager.pause();
    }

    public void seekTo(int progress) {
        playbackManager.seekTo(progress);
        notificationManager.seekTo();
        prepareEventPlay();
    }

    public boolean isPlaying() {
        return playbackManager.isPlaying();
    }

    public Perform getActiveAudio() {
        return activeAudio;
    }

    public PlaybackStatus getPlaybackStatus() {
        return notificationManager.getStatus();
    }

    public void setActiveAudio(Perform newAudio, boolean isContinue) {
        activeAudio = newAudio;

        notificationManager.setActiveAudio(activeAudio);
        initPlayer(isContinue);
        notificationManager.buildNotification(notificationManager.getStatus());

        prepareEventSelect();
    }

    public void setPlayerListener(PlayerStateListener playerStateListener) {
        this.playerStateListener = playerStateListener;
        playbackManager.setPlayerStateListener(playerStateListener);
    }

    public void initMediaSession() {
        notificationManager.initMediaSession(mediaSessionCallback);
    }

    //==============================================================================================
    // API
    //==============================================================================================

    private void sentEvent(EventRequest request) {
        if(activeAudio == null) return;
        request.setId(activeAudio .getId());
        statisticRepository.sentEvent(request, this::endRequest, this::endRequest);
    }

    private void likePerform() {
        if (activeAudio == null) return;

        favoritesRepository.like(activeAudio.getId(),
                null,
                this::proceedSuccessFavorite,
                error -> AlertManager.showAlertMessage(this, "Произошла ошибка"),
                null);
    }

    //==============================================================================================
    // Private
    //==============================================================================================

    private void prepareEventSelect() {
        sentEvent(new EventRequest(EventRequest.Kind.SELECT));
    }

    private void prepareEventPlay() {
        sentEvent(new EventRequest(EventRequest.Kind.PLAY, playbackManager.getPosition()));
    }

    private void prepareEventPause() {
        sentEvent(new EventRequest(EventRequest.Kind.PAUSE, playbackManager.getPosition()));
    }

    private void prepareEventStop() {
        sentEvent(new EventRequest(EventRequest.Kind.STOP, playbackManager.getPosition()));
    }

    private void prepareEventUnexpectedStop() {
        sentEvent(new EventRequest(EventRequest.Kind.UNEXPECTED_STOP, playbackManager.getPosition()));
    }

    private void prepareEventFinish() {
        sentEvent(new EventRequest(EventRequest.Kind.FINISH));
    }

    private void endRequest(Object o) {

    }

    private void proceedSuccessFavorite(Void v) {
        activeAudio.setFavorite(true);
        notificationManager.setFavorite();
        playerStateListener.onFavorite();
    }

    public class PlayerBinder extends Binder {
        public PlayerService getService() {
            return PlayerService.this;
        }
    }
}
