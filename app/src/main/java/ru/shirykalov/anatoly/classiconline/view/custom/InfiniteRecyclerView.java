package ru.shirykalov.anatoly.classiconline.view.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class InfiniteRecyclerView extends RecyclerView {

    private int previousTotal = 0;
    private boolean isLoading = true;

    private ScrollListener listener;

    public InfiniteRecyclerView(Context context) {
        super(context);
    }

    public InfiniteRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public InfiniteRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);

        final int totalItemCount = getLayoutManager().getItemCount();
        final int firstVisibleItem = (((LinearLayoutManager) getLayoutManager())).findFirstVisibleItemPosition();
        final int visibleThreshold = 3;

        if (isLoading) {
            if (totalItemCount > previousTotal) {
                isLoading = false;
                previousTotal = totalItemCount;
            }
        }

        if (!isLoading && dy != 0 && totalItemCount - getChildCount() <= firstVisibleItem + visibleThreshold) {
            listener.onLoadMore();
            isLoading = true;
        }
    }

    public void setListener(ScrollListener listener) {
        this.listener = listener;
    }

    public void clearIndexes() {
        previousTotal = 0;
    }

    public interface ScrollListener {
        void onLoadMore();
    }
}
