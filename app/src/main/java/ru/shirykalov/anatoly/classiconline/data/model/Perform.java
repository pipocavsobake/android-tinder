package ru.shirykalov.anatoly.classiconline.data.model;

import android.net.Uri;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Perform implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("classic_online_id")
    @Expose
    private String onlineId;

    @SerializedName("upload_date")
    @Expose
    private Date date;

    @SerializedName("has_audio")
    @Expose
    private boolean hasAudio;

    @SerializedName("user")
    @Expose
    private String user;

    @SerializedName("likes")
    @Expose
    private int likes;

    @SerializedName("piece")
    @Expose
    private String piece;

    @SerializedName("composer")
    @Expose
    private String composer;

    @SerializedName("performers")
    @Expose
    private List<String> performers;

    @SerializedName("directories")
    @Expose
    private List<Directory> directories = new ArrayList<>();

    @SerializedName("comments_count")
    @Expose
    private int commentsCount;

    @SerializedName("is_favorite")
    @Expose
    private boolean isFavorite;

    @SerializedName("last_ms")
    @Expose
    private Long lastMs;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isHasAudio() {
        return hasAudio;
    }

    public void setHasAudio(boolean hasAudio) {
        this.hasAudio = hasAudio;
    }

    public String getOnlineId() {
        return onlineId;
    }

    public void setOnlineId(String onlineId) {
        this.onlineId = onlineId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public List<String> getPerformers() {
        return performers;
    }

    public void setPerformers(List<String> performers) {
        this.performers = performers;
    }

    public String getPerformersString() {
        return TextUtils.join(", ", performers);
    }

    public String getNotificationTitle() {
        final StringBuilder sb = new StringBuilder();
        final String performers = getPerformersString();

        if (performers.length() >= 20) {
            sb.append(performers, 0, 20).append("...");
        } else {
            sb.append(performers);
        }

        sb.append(" - ");

        if (composer.length() >= 20) {
            sb.append(composer, 0, 20).append("...");
        } else {
            sb.append(composer);
        }

        return sb.toString();
    }

    public List<Directory> getDirectories() {
        return directories;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Long getLastMs() {
        return lastMs;
    }

    public void setLastMs(Long lastMs) {
        this.lastMs = lastMs;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public Uri href() {
        return Uri.parse(String.format("https://classic-online.ru/archive/?file_id=%s", onlineId));
    }

    public Uri getOnlineUri() {
        return Uri.parse(String.format("https://classic-online.ru/a.php?file_id=%s", onlineId));
    }
}
