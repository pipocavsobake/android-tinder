package ru.shirykalov.anatoly.classiconline.view.screens.directories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Directory;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.data.model.result.DirectoriesResult;
import ru.shirykalov.anatoly.classiconline.data.repository.favorites.FavoritesRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.favorites.FavoritesRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.view.screens.favorites.adapter.directories.DirectoriesAdapter;

import static com.google.android.flexbox.FlexDirection.ROW;
import static com.google.android.flexbox.FlexWrap.WRAP;

public class DirectoriesActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvComposer)
    TextView tvComposer;

    @BindView(R.id.etName)
    AutoCompleteTextView etName;

    @BindView(R.id.tvPiece)
    TextView tvPiece;

    @BindView(R.id.tvPerformers)
    TextView tvPerformers;

    @BindView(R.id.rvDirectories)
    RecyclerView rvDirectories;

    private FavoritesRepository repository;

    private DirectoriesAdapter adapterDirectories;
    private ArrayAdapter<String> adapterPopup;

    private Perform perform;

    private List<Directory> directories = new ArrayList<>();

    public static void start(Context ctx, Perform perform) {
        Intent intent = new Intent(ctx, DirectoriesActivity.class);
        intent.putExtra("performId", perform.getId());
        ctx.startActivity(intent);
    }


    //==============================================================================================
    // Android
    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directories);
        repository = new FavoritesRepositoryImpl(this);
        ButterKnife.bind(this);
        setupToolbar();
        setupUI();
        fetchPerform();
    }

    @OnClick(R.id.btnAdd)
    public void onAddClick(View view) {
        createDirectory(etName.getText().toString());
    }

    //==============================================================================================
    // API
    //==============================================================================================

    private void fetchPerform() {
        repository.perform(getIntent().getExtras().getInt("performId"),
                this::showLoading,
                this::proceedSuccessPerform,
                error -> AlertManager.showAlertError(this),
                this::hideLoading);
    }

    private void fetchDirectories() {
        repository.directories(0,
                this::showLoading,
                this::proceedSuccessDirectories,
                error -> AlertManager.showAlertError(this),
                this::hideLoading);
    }

    private void createDirectory(String name) {
        repository.createDirectory(name,
                this::showLoading,
                this::proceedSuccessCreateDirectory,
                this::proceedExistCreateDirectory,
                error -> AlertManager.showAlertError(this),
                this::hideLoading);
    }

    private void addPerformToDirectory(Directory directory) {
        repository.addPerformToDirectory(directory, perform,
                this::showLoading,
                result -> proceedSuccessAddPerform(directory),
                this::proceedExistAddPerformDirectory,
                error -> AlertManager.showAlertError(this),
                this::hideLoading);
    }

    private void removePerformFromDirectory(Directory directory) {
        repository.removePerformFromDirectory(directory, perform,
                this::showLoading,
                result -> proceedSuccessRemovePerform(directory),
                error -> AlertManager.showAlertError(this),
                this::hideLoading);
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void setupToolbar() {
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void setupUI() {
        rvDirectories.setLayoutManager(new FlexboxLayoutManager(this, ROW, WRAP));
    }

    private void showLoading() {
    }

    private void hideLoading() {
    }

    private void proceedSuccessPerform(Perform perform) {
        this.perform = perform;

        tvComposer.setText(perform.getComposer());
        tvPiece.setText(perform.getPiece());
        tvPerformers.setText(TextUtils.join(", ", perform.getPerformers()));

        adapterDirectories = new DirectoriesAdapter(perform.getDirectories(), DirectoriesAdapter.Type.EDIT, new DirectoriesAdapter.OnDirectoryClickListener() {
            @Override
            public void onClick(Directory directory) {

            }

            @Override
            public void onCloseClick(Directory directory) {
                removePerformFromDirectory(directory);
            }
        });
        rvDirectories.setAdapter(adapterDirectories);

        fetchDirectories();
    }

    private void proceedSuccessDirectories(DirectoriesResult result) {
        directories = result.getDirectories();

        updatePopupDirectories();

        etName.setOnItemClickListener((parent, arg1, position, id) -> {
            String selectedName = adapterPopup.getItem(position);
            for (Directory directory : directories) {
                if (directory.getName().equals(selectedName)) {
                    addPerformToDirectory(directory);
                }
            }

            etName.setText("");
        });
    }

    private void proceedSuccessAddPerform(Directory directory) {
        adapterDirectories.addDirectory(directory);
    }

    private void proceedSuccessRemovePerform(Directory directory) {
        adapterDirectories.removeDirectory(directory);
    }

    private void proceedSuccessCreateDirectory(Directory directory) {
        etName.setText("");
        directories.add(directory);
        updatePopupDirectories();
        addPerformToDirectory(directory);
    }

    private void proceedExistCreateDirectory() {
        AlertManager.showAlertMessage(this, "Такая папка уже существует");
    }

    private void proceedExistAddPerformDirectory() {
        AlertManager.showAlertMessage(this, "Эта композиция уже находится в этой папке");
    }

    private void updatePopupDirectories() {
        List<String> names = new ArrayList<>();
        for (Directory directory : directories) {
            names.add(directory.getName());
        }

        adapterPopup = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, names);
        etName.setAdapter(adapterPopup);
    }
}
