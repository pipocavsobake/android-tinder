package ru.shirykalov.anatoly.classiconline.data.model.response;

public class Response<T> {

    public interface Success<T> {
        void onSuccess(T result);
    }

    public interface Error {
        void onError(Throwable error);
    }

    public interface Exist {
        void onExist();
    }

    public interface Start {
        void onStart();
    }

    public interface Complete {
        void onComplete();
    }
}
