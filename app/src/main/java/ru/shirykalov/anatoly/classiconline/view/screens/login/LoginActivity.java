package ru.shirykalov.anatoly.classiconline.view.screens.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.view.screens.settings.SettingsActivity;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.loadingView)
    View loadingView;

    @BindView(R.id.contentView)
    View contentView;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPassword)
    EditText etPassword;

    private AuthRepository authRepository;


    //==============================================================================================
    // Static
    //==============================================================================================

    public static void start(Context ctx) {
        final Intent intent = new Intent(ctx, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ctx.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
    }

    //==============================================================================================
    // Android
    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        authRepository = new AuthRepositoryImpl(this);
        ButterKnife.bind(this);
        setupToolbar();
    }

    @OnClick({R.id.btnSignIn})
    public void onLoginClick(View view) {
        etEmail.setError(null);
        etPassword.setError(null);
        login(etEmail.getText().toString(), etPassword.getText().toString());
    }


    //==============================================================================================
    // API
    //==============================================================================================

    private void login(String email, String password) {
        if (!email.contains("@")) {
            etEmail.setError(getString(R.string.error_invalid_email));
        } else if (password.length() < 4) {
            etPassword.setError(getString(R.string.error_invalid_password));
        } else {
            authRepository.login(email, password, this::startProgress, this::proceedLoginSuccess, this::proceedLoginFailed, this::stopProgress);
        }
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void setupToolbar() {
        toolbar.inflateMenu(R.menu.login);
        toolbar.setOnMenuItemClickListener(item -> {
            SettingsActivity.start(this);
            return false;
        });
    }

    private void startProgress() {
        contentView.setVisibility(GONE);
        loadingView.setVisibility(VISIBLE);
    }

    private void stopProgress() {
        contentView.setVisibility(VISIBLE);
        loadingView.setVisibility(GONE);
    }

    private void proceedLoginSuccess(String token) {
        authRepository.saveToken(token);
        AlertManager.showMessage(this, getString(R.string.login_success));
        finish();
    }

    private void proceedLoginFailed(Throwable error) {
        AlertManager.showAlertError(this, error);
    }
}
