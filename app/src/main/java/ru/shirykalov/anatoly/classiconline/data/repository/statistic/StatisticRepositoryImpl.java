package ru.shirykalov.anatoly.classiconline.data.repository.statistic;

import android.content.Context;
import android.content.SharedPreferences;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.shirykalov.anatoly.classiconline.App;
import ru.shirykalov.anatoly.classiconline.data.model.request.EventRequest;
import ru.shirykalov.anatoly.classiconline.data.model.response.Response;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.SystemUtils;

public class StatisticRepositoryImpl implements StatisticRepository {

    private AuthRepository authRepository;
    private SharedPreferences preferences;


    public StatisticRepositoryImpl(Context ctx) {
        this.authRepository = new AuthRepositoryImpl(ctx);
        this.preferences = SystemUtils.getPreferences(ctx);
    }


    @Override
    public boolean getCheckStatistics() {
        return preferences.getBoolean("statistics", false);
    }

    @Override
    public void setCheckStatistics(Boolean isCheck) {
        preferences.edit().putBoolean("statistics", isCheck).commit();
    }

    @Override
    public void sentEvent(
            EventRequest request,
            Response.Success successListener,
            Response.Error errorListener
    ) {
        if (!getCheckStatistics()) return;

        App.getApi().sentEvent(request, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successListener::onSuccess, errorListener::onError);
    }
}
