package ru.shirykalov.anatoly.classiconline.data.repository.statistic;

import ru.shirykalov.anatoly.classiconline.data.model.request.EventRequest;
import ru.shirykalov.anatoly.classiconline.data.model.response.Response;

public interface StatisticRepository {

    boolean getCheckStatistics();

    void setCheckStatistics(Boolean isCheck);

    void sentEvent(EventRequest request,
                   Response.Success successListener,
                   Response.Error errorListener);
}



