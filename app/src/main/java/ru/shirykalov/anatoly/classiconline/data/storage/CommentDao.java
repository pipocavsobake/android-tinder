package ru.shirykalov.anatoly.classiconline.data.storage;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.shirykalov.anatoly.classiconline.data.model.Comment;

@Dao
public interface CommentDao {

    @Query("SELECT * FROM comment WHERE isApproved = 0 AND isDeclined = 0")
    List<Comment> getNewComments();

    @Query("SELECT * FROM comment WHERE isApproved = 1 OR isDeclined = 1")
    List<Comment> getUnsentComments();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Comment comment);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Comment> comments);

    @Update
    void update(Comment comment);

    @Delete
    void delete(Comment comment);

    @Query("DELETE FROM comment WHERE id = :id")
    void deleteById(long id);

    @Delete
    void delete(List<Comment> comments);
}
