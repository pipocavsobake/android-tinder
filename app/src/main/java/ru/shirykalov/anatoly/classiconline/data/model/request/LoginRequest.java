package ru.shirykalov.anatoly.classiconline.data.model.request;

import com.google.gson.annotations.SerializedName;

public class LoginRequest {

    @SerializedName("data")
    private LoginData data;


    public LoginRequest(String login, String password) {
        this.data = new LoginData(login, password);
    }


    private class LoginData {

        @SerializedName("login")
        private String login;

        @SerializedName("password")
        private String password;

        LoginData(String login, String password) {
            this.login = login;
            this.password = password;
        }
    }
}
