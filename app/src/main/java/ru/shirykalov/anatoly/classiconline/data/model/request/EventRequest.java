package ru.shirykalov.anatoly.classiconline.data.model.request;

import com.google.gson.annotations.SerializedName;

public class EventRequest {

    @SerializedName("perform_id")
    private int id;

    @SerializedName("kind")
    private String kind;

    @SerializedName("value")
    private long value;

    @SerializedName("timestamp")
    private long timestamp;


    public EventRequest(Kind kind) {
        this.timestamp = System.currentTimeMillis();
        this.kind = kind.name();
        this.value = 0;
    }

    public EventRequest(Kind kind, long value) {
        this.timestamp = System.currentTimeMillis();
        this.value = value;
        this.kind = kind.name();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public enum Kind {
        SELECT, PLAY, PAUSE, STOP, UNEXPECTED_STOP, FINISH
    }

}
