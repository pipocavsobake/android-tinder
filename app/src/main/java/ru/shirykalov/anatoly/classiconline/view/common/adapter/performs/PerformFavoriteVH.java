package ru.shirykalov.anatoly.classiconline.view.common.adapter.performs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayoutManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.utils.Utils;
import ru.shirykalov.anatoly.classiconline.view.screens.directories.DirectoriesActivity;
import ru.shirykalov.anatoly.classiconline.view.screens.favorites.adapter.directories.DirectoriesAdapter;
import ru.shirykalov.anatoly.classiconline.view.screens.main.MainActivity;

import static com.google.android.flexbox.FlexDirection.ROW;
import static com.google.android.flexbox.FlexWrap.WRAP;
import static ru.shirykalov.anatoly.classiconline.view.screens.favorites.adapter.directories.DirectoriesAdapter.Type.*;

public class PerformFavoriteVH extends RecyclerView.ViewHolder {

    @BindView(R.id.directories)
    public RecyclerView directories;

    @BindView(R.id.btnFolders)
    public Button btnFolders;

    @BindView(R.id.tvComposer)
    TextView tvComposer;

    @BindView(R.id.tvPiece)
    TextView tvPiece;

    @BindView(R.id.tvPerformers)
    TextView tvPerformers;

    @BindView(R.id.tvUploadDate)
    TextView tvUploadDate;

    @BindView(R.id.tvCountLikes)
    TextView tvCountLikes;

    @BindView(R.id.tvCountComments)
    TextView tvCountComments;

    @BindView(R.id.btnContinue)
    TextView btnContinue;

    @BindView(R.id.btnPlay)
    ImageButton btnPlay;

    @BindView(R.id.btnBrowser)
    ImageButton btnBrowser;

    DirectoriesAdapter.OnDirectoryClickListener listener;

    PerformFavoriteVH(View itemView, DirectoriesAdapter.OnDirectoryClickListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
    }

    public void setData(Perform perform, Context ctx) {
        tvComposer.setText(perform.getComposer());
        tvPiece.setText(perform.getPiece());
        tvPerformers.setText(TextUtils.join(", ", perform.getPerformers()));
        tvUploadDate.setText(DateUtils.formatDateTime(ctx, perform.getDate().getTime(), 0));
        tvCountLikes.setText(perform.getLikes() + "");
        tvCountComments.setText(perform.getCommentsCount() + "");
        tvCountLikes.setBackgroundTintList(Utils.isFavoriteColor(ctx, perform.isFavorite()));
        btnContinue.setVisibility(perform.getLastMs() > 0 ? View.VISIBLE : View.GONE);

        directories.setLayoutManager(new FlexboxLayoutManager(ctx, ROW, WRAP));
        directories.setAdapter(new DirectoriesAdapter(perform.getDirectories(), NORMAL, listener));
        btnFolders.setOnClickListener(v -> DirectoriesActivity.start(ctx, perform));

        btnPlay.setOnClickListener(v -> onPlayClick(perform, false, ctx));
        btnBrowser.setOnClickListener(view -> Utils.openBrowser(ctx, perform));
        btnContinue.setOnClickListener(v -> onPlayClick(perform, true, ctx));
    }

    private void onPlayClick(Perform perform, boolean isContinue, Context ctx) {
        if (perform.isHasAudio()) {
            ((MainActivity) ctx).startPlayer(perform, isContinue);
        } else {
            AlertManager.showMessage(ctx, "Это видео");
        }
    }
}
