package ru.shirykalov.anatoly.classiconline.view.screens.favorites.adapter.directories;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Directory;
import ru.shirykalov.anatoly.classiconline.utils.Utils;

public class DirectoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Type type;
    private OnDirectoryClickListener listener;
    private List<Directory> directories;

    public DirectoriesAdapter(List<Directory> directories, Type type, OnDirectoryClickListener listener) {
        this.directories = directories;
        this.type = type;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (type) {
            case EDIT:
                return new DirectoryEditVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_directory_edit, parent, false), listener);
            default:
                return new DirectoryVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_directory, parent, false), listener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DirectoryVH) {
            ((DirectoryVH) holder).setDirectory(directories.get(position));
        }

        if (holder instanceof DirectoryEditVH) {
            ((DirectoryEditVH) holder).setDirectory(directories.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return directories.size();
    }

    public void addDirectory(Directory directory) {
        directories.add(0, directory);
        notifyItemInserted(0);
    }

    public void removeDirectory(Directory directory) {
        int index = directories.indexOf(directory);
        directories.remove(directory);
        notifyItemRemoved(index);
    }

    public enum Type {
        NORMAL, EDIT
    }

    public interface OnDirectoryClickListener {
        void onClick(Directory directory);
        void onCloseClick(Directory directory);
    }
}
