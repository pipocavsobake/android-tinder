package ru.shirykalov.anatoly.classiconline.data.repository.comments;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.shirykalov.anatoly.classiconline.App;
import ru.shirykalov.anatoly.classiconline.data.model.Comment;
import ru.shirykalov.anatoly.classiconline.data.model.request.CommentRequest;
import ru.shirykalov.anatoly.classiconline.data.model.response.Response;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.SystemUtils;

public class CommentsRepositoryImpl implements CommentsRepository {

    private AuthRepository authRepository;
    private SharedPreferences preferences;

    public CommentsRepositoryImpl(Context ctx) {
        this.authRepository = new AuthRepositoryImpl(ctx);
        this.preferences = SystemUtils.getPreferences(ctx);
    }

    @Override
    public void comments(
            Response.Start startListener,
            Response.Success<List<Comment>> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().comments(authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> startListener.onStart())
                .doFinally(completeListener::onComplete)
                .subscribe(
                        result -> successListener.onSuccess(result.body().getComments()),
                        errorListener::onError
                );
    }

    @Override
    public void commentApprove(Comment comment) {
        sendCommentAction(comment, new CommentRequest(comment.getId(), true));
    }

    @Override
    public void commentDecline(Comment comment) {
        sendCommentAction(comment, new CommentRequest(comment.getId(), false));
    }

    @Override
    public void sendUnsentComments() {
        final List<Comment> comments = App.getInstance().getDao().getUnsentComments();

        if (comments.isEmpty()) return;

        final List<Long> commentIdsApproved = new ArrayList<>();
        final List<Long> commentIdsDeclined = new ArrayList<>();

        for (Comment comment : comments) {
            if (comment.isApproved()) commentIdsApproved.add(comment.getId());
            if (comment.isDeclined()) commentIdsDeclined.add(comment.getId());
        }

        final CommentRequest request = new CommentRequest(commentIdsApproved, commentIdsDeclined);

        App.getApi().sentComments(request, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        result -> App.getInstance().getDao().delete(comments),
                        error -> {
                        });
    }

    private void sendCommentAction(Comment comment, CommentRequest request) {
        App.getApi().sentComments(request, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        result -> App.getInstance().getDao().delete(comment),
                        error -> App.getInstance().getDao().update(comment));
    }

    @Override
    public boolean hasOfflineComments() {
        return !App.getInstance().getDao().getNewComments().isEmpty();
    }

    @Override
    public void insertOfflineComments(List<Comment> comments) {
        App.getInstance().getDao().insert(comments);
    }

    @Override
    public List<Comment> getOfflineComments() {
        return App.getInstance().getDao().getNewComments();
    }
}
