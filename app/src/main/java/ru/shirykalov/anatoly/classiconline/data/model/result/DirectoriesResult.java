package ru.shirykalov.anatoly.classiconline.data.model.result;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import ru.shirykalov.anatoly.classiconline.data.model.Directory;

public class DirectoriesResult {

    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("data")
    @Expose
    private List<Directory> directories;

    public int getTotal() {
        return total;
    }

    public String getTextTotal() {
        return String.valueOf(total);
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Directory> getDirectories() {
        return directories;
    }

    public void setData(List<Directory> directories) {
        this.directories = directories;
    }
}
