package ru.shirykalov.anatoly.classiconline.view.screens.favorites;

import android.support.v7.util.DiffUtil;

import java.util.List;

import ru.shirykalov.anatoly.classiconline.data.model.Perform;

public class PerformDiffUtilCallback extends DiffUtil.Callback {
 
   private final List<Perform> oldList;
   private final List<Perform> newList;
 
   public PerformDiffUtilCallback(List<Perform> oldList, List<Perform> newList) {
       this.oldList = oldList;
       this.newList = newList;
   }
 
   @Override
   public int getOldListSize() {
       return oldList.size();
   }
 
   @Override
   public int getNewListSize() {
       return newList.size();
   }
 
   @Override
   public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
       return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
   }
 
   @Override
   public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
       return oldList.get(oldItemPosition).getDirectories().equals(newList.get(newItemPosition).getDirectories());
   }
}