/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shirykalov.anatoly.classiconline.player;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.Map;

import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;

class PlaybackManager implements MediaPlayer.OnCompletionListener {

    private final Context context;

    private SimpleExoPlayer player;

    private AuthRepository authRepository;
    private PlayerStateListener playerStateListener;
    private PlayerStatisticsListener statisticsListener;

    private long resumePosition;
    private boolean isNewPerform;

    PlaybackManager(Context context, PlayerStatisticsListener statisticsListener) {
        this.context = context;
        this.authRepository = new AuthRepositoryImpl(context);
        this.statisticsListener = statisticsListener;
    }

    void setPlayerStateListener(PlayerStateListener playerStateListener) {
        this.playerStateListener = playerStateListener;
    }

    boolean isPlaying() {
        return player != null && player.getPlayWhenReady();
    }

    int getPosition() {
        return player != null ? (int) player.getCurrentPosition() : 0;
    }

    int getDuration() {
        return player != null ? (int) player.getDuration() : 0;
    }

    void reset(Perform activeAudio, boolean isContinue) {
        stop();

        isNewPerform = true;
        player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), new DefaultTrackSelector(), new DefaultLoadControl());

        DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory(Util.getUserAgent(context, "Clonclient"), null);

        if (authRepository.isNotClonAuthorized()) {
            Toast.makeText(context, "Auth failed", Toast.LENGTH_LONG).show();
            return;
        }

        for (Map.Entry<String, String> entry : authRepository.getClonHeaders().entrySet()) {
            Log.d(entry.getKey(), entry.getValue());
            if (entry.getKey().equalsIgnoreCase("Cookie"))
                dataSourceFactory.setDefaultRequestProperty(entry.getKey(), entry.getValue());
        }


        player.prepare(new ExtractorMediaSource(activeAudio.getOnlineUri(), dataSourceFactory, new DefaultExtractorsFactory(), null, null));
        player.setPlayWhenReady(true);
        player.seekTo(isContinue ? activeAudio.getLastMs() : 0);
        player.addListener(new Player.DefaultEventListener() {

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    if (isNewPerform) {
                        playerStateListener.onInit(activeAudio);
                        isNewPerform = false;
                    }

                    playerStateListener.onPlay();
                } else if (!playWhenReady && playbackState == Player.STATE_READY) {
                    playerStateListener.onPause();
                }else if (playbackState == Player.STATE_ENDED) {
                    playerStateListener.onComplete();
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                Toast.makeText(context, "Play failed", Toast.LENGTH_LONG).show();
                playerStateListener.onComplete();
                statisticsListener.onError();
            }
        });
    }

    void pause() {
        playerStateListener.onPause();
        player.setPlayWhenReady(false);
        resumePosition = player.getCurrentPosition();
    }

    void release() {
        if (player != null) {
            player.stop(true);
            player.release();
        }
    }

    void resume() {
        playerStateListener.onPlay();
        player.seekTo(resumePosition);
        player.setPlayWhenReady(true);
    }

    void seekTo(int progress) {
        resumePosition = progress;
        player.seekTo(progress);
    }

    private void stop() {
        if (player != null) player.stop(true);
    }

    @Override
    public void onCompletion(MediaPlayer player) {
        stop();
        statisticsListener.onCompletion();
    }
}
