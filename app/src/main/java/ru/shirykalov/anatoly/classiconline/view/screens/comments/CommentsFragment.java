package ru.shirykalov.anatoly.classiconline.view.screens.comments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Comment;
import ru.shirykalov.anatoly.classiconline.data.repository.comments.CommentsRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.comments.CommentsRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.view.custom.TinderCommentCard;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CommentsFragment extends Fragment implements CommentsListener {

    @BindView(R.id.contentView)
    SwipePlaceHolderView contentView;

    @BindView(R.id.loadingView)
    View loadingView;

    private CommentsRepository commentsRepository;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comments, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        commentsRepository = new CommentsRepositoryImpl(getActivity());
        setupUI();
        fetchData();
    }


    //==============================================================================================
    // API
    //==============================================================================================

    private void fetchData() {
        commentsRepository.comments(this::startProgress, this::proceedCommentsSuccess, this::proceedCommentsFailed, this::stopProgress);
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void setupUI() {
        contentView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.view_tinder_swipe_in_msg)
                        .setSwipeOutMsgLayoutId(R.layout.view_tinder_swipe_out_msg));

        contentView.addItemRemoveListener(count -> {
            if (count <= 3) fetchData();
        });
    }

    private void startProgress() {
        if (contentView.getChildCount() != 0) return;
        contentView.setVisibility(VISIBLE);
        loadingView.setVisibility(GONE);
    }

    private void stopProgress() {
        if (contentView.getChildCount() != 0) return;
        contentView.setVisibility(VISIBLE);
        loadingView.setVisibility(GONE);
    }

    private void proceedCommentsSuccess(List<Comment> comments) {
        commentsRepository.insertOfflineComments(comments);

        for (Comment comment : comments) {
            contentView.addView(new TinderCommentCard(comment, this));
        }
    }

    private void proceedCommentsFailed(Throwable error) {
        if (commentsRepository.hasOfflineComments()) {
            proceedCommentsSuccess(commentsRepository.getOfflineComments());
        } else {
            AlertManager.showAlertError(getContext(), error);
        }
    }

    @Override
    public void onCommentApproved(Comment comment) {
        commentsRepository.commentApprove(comment);
    }

    @Override
    public void onCommentDeclined(Comment comment) {
        commentsRepository.commentDecline(comment);
    }
}
