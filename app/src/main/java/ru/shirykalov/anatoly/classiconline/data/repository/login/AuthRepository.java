package ru.shirykalov.anatoly.classiconline.data.repository.login;

import java.util.Map;

import ru.shirykalov.anatoly.classiconline.data.model.response.Response;

public interface AuthRepository {

    String getBaseHost();

    void setBaseHost(String host);

    void login(String email,
               String password,
               Response.Start startListener,
               Response.Success<String> successListener,
               Response.Error errorListener,
               Response.Complete completeListener);

    String getToken();

    boolean saveClonHeaders(Map<String, String> headers);

    Map<String, String> getClonHeaders();

    Map<String, String> getHeaders();

    boolean saveToken(String token);

    boolean isAuthorized();

    void resetAuthorized();

    boolean isNotClonAuthorized();
}
