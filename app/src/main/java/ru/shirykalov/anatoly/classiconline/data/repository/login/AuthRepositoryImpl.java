package ru.shirykalov.anatoly.classiconline.data.repository.login;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.AuthFailureError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.shirykalov.anatoly.classiconline.App;
import ru.shirykalov.anatoly.classiconline.data.model.request.LoginRequest;
import ru.shirykalov.anatoly.classiconline.data.model.response.Response;
import ru.shirykalov.anatoly.classiconline.utils.SystemUtils;
import ru.shirykalov.anatoly.classiconline.utils.Utils;

public class AuthRepositoryImpl implements AuthRepository {

    private SharedPreferences preferences;


    public AuthRepositoryImpl(Context ctx) {
        this.preferences = SystemUtils.getPreferences(ctx);
    }


    @Override
    public String getBaseHost() {
        return preferences.getString("host", "clonclient.shirykalov.ru");
    }

    @Override
    public void setBaseHost(String host) {
        preferences.edit().putString("host", host).commit();
        resetAuthorized();
    }

    @Override
    public void login(
            String email,
            String password,
            Response.Start startListener,
            Response.Success<String> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().login(new LoginRequest(email, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(d -> startListener.onStart())
                .doFinally(() -> completeListener.onComplete())
                .subscribe(
                        result -> successListener.onSuccess(result.getToken()),
                        errorListener::onError
                );
    }

    @Override
    public boolean isAuthorized() {
        return getToken().isEmpty();
    }

    @Override
    public boolean isNotClonAuthorized() {
        return getClonHeaders() == null || getClonHeaders().isEmpty();
    }

    @Override
    public void resetAuthorized() {
        saveToken("");
    }

    public Map<String, String> getHeaders() {
        Map<String, String> params = new HashMap<>();
        params.put("Authorization", getToken());
        return params;
    }

    public boolean saveToken(String token) {
        return preferences.edit().putString("token", token).commit();
    }

    public String getToken() {
        return preferences.getString("token", "");
    }

    public Map<String, String> getClonHeaders() {
        String clonHeaders = preferences.getString("clon_headers", "");
        return new Gson().fromJson(clonHeaders, new TypeToken<Map<String, String>>() {
        }.getType());
    }

    public boolean saveClonHeaders(Map<String, String> headers) {
        return preferences.edit().putString("clon_headers", new Gson().toJson(headers)).commit();
    }
}
