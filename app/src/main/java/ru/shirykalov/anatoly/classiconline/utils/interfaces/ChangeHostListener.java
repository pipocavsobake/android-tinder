package ru.shirykalov.anatoly.classiconline.utils.interfaces;

public interface ChangeHostListener {
    void onChangeHost(String host);
}
