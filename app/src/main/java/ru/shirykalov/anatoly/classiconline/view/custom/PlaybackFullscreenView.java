package ru.shirykalov.anatoly.classiconline.view.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.player.PlaybackStatus;
import ru.shirykalov.anatoly.classiconline.player.PlayerViewListener;
import ru.shirykalov.anatoly.classiconline.utils.Utils;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.graphics.Color.RED;
import static android.graphics.Color.WHITE;
import static android.graphics.PorterDuff.Mode.SRC_IN;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_favorite_default;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_favorite_selected;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_pause;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_play;

public class PlaybackFullscreenView extends LinearLayout {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvArtist)
    TextView tvArtist;

    @BindView(R.id.tvPerformers)
    TextView tvPerformers;

    @BindView(R.id.tvCurrentTime)
    TextView tvCurrentTime;

    @BindView(R.id.tvDuration)
    TextView tvDuration;

    @BindView(R.id.progressSong)
    SeekBar progressSong;

    @BindView(R.id.btnBrowse)
    ImageView btnBrowse;

    @BindView(R.id.btnPlayPause)
    ImageView btnPlayPause;

    @BindView(R.id.btnFavorite)
    ImageView btnFavorite;

    @BindView(R.id.btnClose)
    ImageView btnClose;

    private PlayerViewListener viewListener;

    private Perform currentAudio;
    private int currentDuration;

    private Time durationType = Time.DEFAULT;

    private boolean isSeekable = false;

    public PlaybackFullscreenView(Context context) {
        super(context);
        init();
    }

    public PlaybackFullscreenView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlaybackFullscreenView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setListener(PlayerViewListener listener) {
        viewListener = listener;
    }

    public void setPerform(Perform perform, int resumePosition, int duration) {
        currentAudio = perform;
        currentDuration = duration;
        tvArtist.setText(perform.getComposer());
        tvPerformers.setText(perform.getPerformersString());
        tvTitle.setText(perform.getPiece());
        progressSong.setProgress(resumePosition);
        progressSong.setMax(duration);
        tvDuration.setText(Utils.toDurationTime(duration));

        btnFavorite.setEnabled(!perform.isFavorite());
        btnFavorite.setImageResource(perform.isFavorite() ? ic_favorite_selected : ic_favorite_default);
        btnFavorite.getDrawable().setColorFilter(perform.isFavorite() ? RED : WHITE, SRC_IN);

        tvCurrentTime.setOnClickListener(v -> {
            switch (durationType) {
                case DEFAULT:
                    durationType = Time.REVERSE;
                    break;
                case REVERSE:
                    durationType = Time.DEFAULT;
                    break;
            }
        });
    }

    public void play() {
        btnPlayPause.setImageResource(ic_pause);
    }

    public void pause() {
        btnPlayPause.setImageResource(ic_play);
    }

    public void stop() {
        setVisibility(GONE);
    }

    public void setCurrentPosition(int resumePosition) {
        progressSong.setProgress(resumePosition);

        switch (durationType) {
            case DEFAULT:
                tvCurrentTime.setText(Utils.toDurationTime(resumePosition));
                break;
            case REVERSE:
                tvCurrentTime.setText(String.format("-%s", Utils.toDurationTime(currentDuration - resumePosition)));
                break;
        }
    }

    public void setFavorite() {
        btnFavorite.setEnabled(false);
        btnFavorite.setImageResource(ic_favorite_selected);
        btnFavorite.getDrawable().setColorFilter(RED, SRC_IN);
    }

    public void setStatus(PlaybackStatus playbackStatus) {
        switch (playbackStatus) {
            case PLAYING:
                play();
                break;
            case PAUSED:
                pause();
                break;
        }
    }

    //==============================================================================================
    // Private
    //==============================================================================================

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_playback_controls_fullsceen, this, true);

        ButterKnife.bind(this);

        btnClose.setOnClickListener(v -> viewListener.onCloseFullscreenPlayer());
        btnBrowse.setOnClickListener(v -> Utils.openBrowser(getContext(), currentAudio));
        btnPlayPause.setOnClickListener(v -> viewListener.onPlayPauseClick());
        btnFavorite.setOnClickListener(v -> viewListener.onFavoriteClick(currentAudio));

        progressSong.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean isUser) {
                if (isUser && !isSeekable) {
                    isSeekable = true;
                    viewListener.startSeek();
                }

                if (isUser && isSeekable) {
                    tvCurrentTime.setText(Utils.toDurationTime(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isSeekable = false;
                viewListener.endSeek(seekBar.getProgress());
            }
        });
    }

    enum Time {
        DEFAULT, REVERSE
    }
}
