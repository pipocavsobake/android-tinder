package ru.shirykalov.anatoly.classiconline.view.screens.comments;

import ru.shirykalov.anatoly.classiconline.data.model.Comment;

public interface CommentsListener {

    void onCommentApproved(Comment comment);

    void onCommentDeclined(Comment comment);
}
