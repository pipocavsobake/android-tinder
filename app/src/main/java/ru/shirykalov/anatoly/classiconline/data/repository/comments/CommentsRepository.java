package ru.shirykalov.anatoly.classiconline.data.repository.comments;

import java.util.List;

import ru.shirykalov.anatoly.classiconline.data.model.Comment;
import ru.shirykalov.anatoly.classiconline.data.model.response.Response;

public interface CommentsRepository {

    void comments(Response.Start startListener,
                  Response.Success<List<Comment>> successListener,
                  Response.Error errorListener,
                  Response.Complete completeListener);

    boolean hasOfflineComments();

    void insertOfflineComments(List<Comment> comments);

    List<Comment> getOfflineComments();

    void commentApprove(Comment comment);

    void commentDecline(Comment comment);

    void sendUnsentComments();
}
