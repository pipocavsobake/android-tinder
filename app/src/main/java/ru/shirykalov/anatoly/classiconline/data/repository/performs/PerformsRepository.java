package ru.shirykalov.anatoly.classiconline.data.repository.performs;

import ru.shirykalov.anatoly.classiconline.data.model.response.Response;
import ru.shirykalov.anatoly.classiconline.data.model.result.PerformsResult;

public interface PerformsRepository {

    void recentPerforms(int offset,
                        int comments,
                        int likes,
                        boolean video,
                        Response.Start startListener,
                        Response.Success<PerformsResult> successListener,
                        Response.Error errorListener,
                        Response.Complete completeListener);

    void searchPerforms(String query,
                        int offset,
                        Response.Start startListener,
                        Response.Success<PerformsResult> successListener,
                        Response.Error errorListener,
                        Response.Complete completeListener);
}
