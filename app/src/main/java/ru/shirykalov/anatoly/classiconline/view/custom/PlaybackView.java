package ru.shirykalov.anatoly.classiconline.view.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.player.PlaybackStatus;
import ru.shirykalov.anatoly.classiconline.player.PlayerViewListener;
import ru.shirykalov.anatoly.classiconline.utils.Utils;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.graphics.Color.RED;
import static android.graphics.Color.WHITE;
import static android.graphics.PorterDuff.Mode.SRC_IN;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_favorite_default;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_favorite_selected;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_pause;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_play;

public class PlaybackView extends LinearLayout {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvArtist)
    TextView tvArtist;

    @BindView(R.id.tvPerformers)
    TextView tvPerformers;

    @BindView(R.id.progressSong)
    ProgressBar progressSong;

    @BindView(R.id.ivPlayPause)
    ImageView ivPlayPause;

    @BindView(R.id.ivFavorite)
    ImageView ivFavorite;

    @BindView(R.id.btnClose)
    ImageView btnClose;

    private PlayerViewListener viewListener;

    private Perform currentAudio;


    public PlaybackView(Context context) {
        super(context);
        init();
    }

    public PlaybackView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlaybackView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    public void setListener(PlayerViewListener listener) {
        viewListener = listener;
    }

    public void setPerform(Perform perform, int resumePosition, int duration) {
        currentAudio = perform;
        tvArtist.setText(perform.getComposer());
        tvPerformers.setText(perform.getPerformersString());
        tvTitle.setText(perform.getPiece());
        progressSong.setProgress(resumePosition);
        progressSong.setMax(duration);

        ivFavorite.setEnabled(!perform.isFavorite());
        ivFavorite.setImageResource(perform.isFavorite() ? ic_favorite_selected : ic_favorite_default);
        ivFavorite.getDrawable().setColorFilter(perform.isFavorite() ? RED : WHITE, SRC_IN);
    }

    public void play() {
        ivPlayPause.setImageResource(ic_pause);
    }

    public void pause() {
        ivPlayPause.setImageResource(ic_play);
    }

    public void stop() {
        setVisibility(GONE);
    }

    public void setCurrentPosition(int resumePosition) {
        progressSong.setProgress(resumePosition);
    }

    public void setFavorite() {
        ivFavorite.setEnabled(false);
        ivFavorite.setImageResource(ic_favorite_selected);
        ivFavorite.getDrawable().setColorFilter(RED, SRC_IN);
    }

    public void setStatus(PlaybackStatus playbackStatus) {
        switch (playbackStatus) {
            case PLAYING:
                play();
                break;
            case PAUSED:
                pause();
                break;
        }
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_playback_controls, this, true);
        ButterKnife.bind(this);
        ivPlayPause.setOnClickListener(v -> viewListener.onPlayPauseClick());
        ivFavorite.setOnClickListener(v -> viewListener.onFavoriteClick(currentAudio));
        btnClose.setOnClickListener(v -> viewListener.onStopClick());
    }
}
