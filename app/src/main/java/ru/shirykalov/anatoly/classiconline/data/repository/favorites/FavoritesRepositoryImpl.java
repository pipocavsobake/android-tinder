package ru.shirykalov.anatoly.classiconline.data.repository.favorites;

import android.content.Context;
import android.content.SharedPreferences;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.shirykalov.anatoly.classiconline.App;
import ru.shirykalov.anatoly.classiconline.data.model.Directory;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.data.model.request.CreateDirectoryRequest;
import ru.shirykalov.anatoly.classiconline.data.model.response.Response;
import ru.shirykalov.anatoly.classiconline.data.model.result.DirectoriesResult;
import ru.shirykalov.anatoly.classiconline.data.model.result.PerformsResult;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.SystemUtils;

public class FavoritesRepositoryImpl implements FavoritesRepository {

    private AuthRepository authRepository;
    private SharedPreferences sharedPreferences;

    public FavoritesRepositoryImpl(Context ctx) {
        this.authRepository = new AuthRepositoryImpl(ctx);
        this.sharedPreferences = SystemUtils.getPreferences(ctx);
    }

    public boolean setFavoriteFilterId(int id) {
        return sharedPreferences.edit().putInt("favorite_filter_id", id).commit();
    }

    public int getFavoriteFilterId() {
        return sharedPreferences.getInt("favorite_filter_id", 0);
    }

    @Override
    public void like(
            int performId,
            Response.Start startListener,
            Response.Success<Void> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().like(performId, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        perform -> successListener.onSuccess(null),
                        errorListener::onError
                );
    }

    @Override
    public void perform(
            int performId,
            Response.Start startListener,
            Response.Success<Perform> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().perform(performId, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> startListener.onStart())
                .doFinally(completeListener::onComplete)
                .subscribe(successListener::onSuccess, errorListener::onError);
    }

    @Override
    public void performs(
            int offset,
            int limit,
            Response.Start startListener,
            Response.Success<PerformsResult> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        if (getFavoriteFilterId() == 0) {
            App.getApi().favorites(offset, limit, authRepository.getHeaders())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> startListener.onStart())
                    .doFinally(completeListener::onComplete)
                    .subscribe(successListener::onSuccess, errorListener::onError);
        } else {
            App.getApi().performsDirectory(getFavoriteFilterId(), offset, limit, authRepository.getHeaders())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> startListener.onStart())
                    .doFinally(completeListener::onComplete)
                    .subscribe(successListener::onSuccess, errorListener::onError);
        }
    }

    @Override
    public void directories(
            int offset,
            Response.Start startListener,
            Response.Success<DirectoriesResult> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().directories(1000, offset, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> startListener.onStart())
                .doFinally(completeListener::onComplete)
                .subscribe(successListener::onSuccess, errorListener::onError);
    }

    @Override
    public void createDirectory(
            String name,
            Response.Start startListener,
            Response.Success<Directory> successListener,
            Response.Exist existListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        CreateDirectoryRequest request = new CreateDirectoryRequest(name);

        App.getApi().createDirectory(request, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> startListener.onStart())
                .doFinally(completeListener::onComplete)
                .subscribe(
                        response -> {
                            switch (response.code()) {
                                case 422:
                                    existListener.onExist();
                                    break;
                                case 200:
                                    successListener.onSuccess(response.body());
                                    break;
                            }
                        },
                        errorListener::onError
                );
    }

    @Override
    public void addPerformToDirectory(
            Directory directory,
            Perform perform,
            Response.Start startListener,
            Response.Success successListener,
            Response.Exist existListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().addPerformToDirectory(directory.getId(), perform.getId(), authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> startListener.onStart())
                .doFinally(completeListener::onComplete)
                .subscribe(
                        response -> {
                            switch (response.code()) {
                                case 422:
                                    existListener.onExist();
                                    break;
                                case 200:
                                    successListener.onSuccess(null);
                                    break;
                            }
                        },
                        error -> errorListener.onError(error)
                );
    }

    @Override
    public void removePerformFromDirectory(
            Directory directory,
            Perform perform,
            Response.Start startListener,
            Response.Success<Boolean> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().removePerformDirectory(directory.getId(), perform.getId(), authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> startListener.onStart())
                .doFinally(completeListener::onComplete)
                .subscribe(
                        directoriesResult -> successListener.onSuccess(true),
                        error -> errorListener.onError(error)
                );
    }
}
