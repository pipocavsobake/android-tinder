package ru.shirykalov.anatoly.classiconline.data.model.request;

import com.google.gson.annotations.SerializedName;

public class CreateDirectoryRequest {

    @SerializedName("name")
    private String name;

    public CreateDirectoryRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
