package ru.shirykalov.anatoly.classiconline.view.screens.recent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.result.PerformsResult;
import ru.shirykalov.anatoly.classiconline.data.repository.performs.PerformsRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.performs.PerformsRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.view.common.adapter.performs.PerformAdapter;
import ru.shirykalov.anatoly.classiconline.view.custom.InfiniteRecyclerView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class RecentAudioFragment extends Fragment {

    @BindView(R.id.etLikes)
    EditText etLikes;

    @BindView(R.id.etComments)
    EditText etComments;

    @BindView(R.id.cbVideo)
    CheckBox cbVideo;

    @BindView(R.id.tvTotal)
    TextView tvTotal;

    @BindView(R.id.contentView)
    InfiniteRecyclerView contentView;

    @BindView(R.id.loadingView)
    View loadingView;

    private PerformsRepository repository;

    private PerformAdapter performsAdapter;


    //==============================================================================================
    // Android
    //==============================================================================================

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recent_audio, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        repository = new PerformsRepositoryImpl(getActivity());
        setupUI();
    }

    @OnClick(R.id.btnShowRecent)
    public void onShowCLick(View view) {
        fetchPerforms();
    }


    //==============================================================================================
    // API
    //==============================================================================================

    private void fetchPerforms() {
        repository.recentPerforms(
                0,
                Integer.parseInt(etComments.getText().toString().isEmpty() ? "0" : etComments.getText().toString()),
                Integer.parseInt(etLikes.getText().toString().isEmpty() ? "0" : etLikes.getText().toString()),
                cbVideo.isChecked(),
                this::startProgress,
                this::proceedPerformsSuccess,
                this::proceedFailed,
                this::stopProgress);
    }

    private void fetchOldPerforms() {
        repository.recentPerforms(
                performsAdapter.getItemCount(),
                Integer.parseInt(etComments.getText().toString().isEmpty() ? "0" : etComments.getText().toString()),
                Integer.parseInt(etLikes.getText().toString().isEmpty() ? "0" : etLikes.getText().toString()),
                cbVideo.isChecked(),
                this::startProgress,
                this::proceedOldPerformsSuccess,
                this::proceedFailed,
                this::stopProgress);
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void setupUI() {
        performsAdapter = new PerformAdapter(getContext());
        contentView.setAdapter(performsAdapter);
        contentView.setListener(this::fetchOldPerforms);
    }

    private void startProgress() {
        loadingView.setVisibility(VISIBLE);
        if (contentView.getChildCount() != 0) return;
        contentView.setVisibility(GONE);
    }

    private void stopProgress() {
        contentView.setVisibility(VISIBLE);
        loadingView.setVisibility(GONE);
    }

    private void proceedPerformsSuccess(PerformsResult result) {
        contentView.clearIndexes();
        tvTotal.setText(String.format(getString(R.string.total), result.getTotal()));
        performsAdapter.setItems(result.getPerforms());
    }

    private void proceedOldPerformsSuccess(PerformsResult result) {
        performsAdapter.appendItems(result.getPerforms());
    }

    private void proceedFailed(Throwable error) {
        AlertManager.showAlertError(getContext(), error);
    }
}
