package ru.shirykalov.anatoly.classiconline.player;

import ru.shirykalov.anatoly.classiconline.data.model.Perform;

public interface PlayerStateListener {

    void onInit(Perform perform);

    void onPlay();

    void onPause();

    void onStop();

    void onFavorite();

    void onComplete();
}
