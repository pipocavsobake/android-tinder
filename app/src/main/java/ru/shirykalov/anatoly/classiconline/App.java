package ru.shirykalov.anatoly.classiconline;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.shirykalov.anatoly.classiconline.data.api.ClonAPI;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.data.storage.AppDatabase;
import ru.shirykalov.anatoly.classiconline.data.storage.CommentDao;

import static com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;

/**
 * Created by Elyseev Vladimir on 24.08.2018.
 */
public class App extends Application {

    public static App instance;

    private AppDatabase database;
    private ClonAPI api;

    public static App getInstance() {
        return instance;
    }

    public static ClonAPI getApi() {
        return instance.api;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, AppDatabase.class, "database").allowMainThreadQueries().build();
        initApi();
    }

    public void initApi() {
        AuthRepository authRepository = new AuthRepositoryImpl(this);

        RxJava2CallAdapterFactory CALL_ADAPTER_FACTORY = RxJava2CallAdapterFactory.create();

        Gson GSON = new GsonBuilder()
                .setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES)
                .create();

        GsonConverterFactory CONVERTER_FACTORY = GsonConverterFactory.create(GSON);

        HttpLoggingInterceptor okHttpInterceptorLogging = new HttpLoggingInterceptor();
        okHttpInterceptorLogging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(okHttpInterceptorLogging)
                .addInterceptor(new OkHttpProfilerInterceptor())
                .readTimeout(60, TimeUnit.SECONDS);

        api = new Retrofit.Builder()
                .addCallAdapterFactory(CALL_ADAPTER_FACTORY)
                .addConverterFactory(CONVERTER_FACTORY)
                .client(builder.build())
                .baseUrl("https://" + authRepository.getBaseHost())
                .build()
                .create(ClonAPI.class);
    }

    public CommentDao getDao() {
        return database.commentDao();
    }
}
