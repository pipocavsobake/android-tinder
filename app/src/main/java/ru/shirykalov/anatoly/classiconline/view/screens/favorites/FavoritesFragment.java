package ru.shirykalov.anatoly.classiconline.view.screens.favorites;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Directory;
import ru.shirykalov.anatoly.classiconline.data.model.result.DirectoriesResult;
import ru.shirykalov.anatoly.classiconline.data.model.result.PerformsResult;
import ru.shirykalov.anatoly.classiconline.data.repository.favorites.FavoritesRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.favorites.FavoritesRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.view.common.adapter.performs.PerformFavoritesAdapter;
import ru.shirykalov.anatoly.classiconline.view.custom.InfiniteRecyclerView;
import ru.shirykalov.anatoly.classiconline.view.screens.favorites.adapter.directories.DirectoriesAdapter;

public class FavoritesFragment extends Fragment {

    @BindView(R.id.contentView)
    View contentView;

    @BindView(R.id.loadingView)
    View loadingView;

    @BindView(R.id.btnFilter)
    Button btnFilter;

    @BindView(R.id.tvTotal)
    TextView tvTotal;

    @BindView(R.id.rvPerforms)
    InfiniteRecyclerView rvPerforms;

    private FavoritesRepository repository;

    private PerformFavoritesAdapter adapter;

    private int limit = 20;

    private List<Directory> directories = new ArrayList<>();


    //==============================================================================================
    // Android
    //==============================================================================================

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorite_audio, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        repository = new FavoritesRepositoryImpl(getActivity());
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        limit = adapter != null ? adapter.getItemCount() : 20;
        fetchDirectories();
    }

    @OnClick(R.id.btnFilter)
    public void onShowFilterClick(View view) {
        AlertManager.showFilter(getActivity(), directories, repository.getFavoriteFilterId(), (dialog, which) -> {
            int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
            repository.setFavoriteFilterId(directories.get(selectedPosition).getId());
            btnFilter.setText(directories.get(selectedPosition).getName());
            fetchFavoritesPerforms();
        });
    }


    //==============================================================================================
    // API
    //==============================================================================================

    private void fetchDirectories() {
        repository.directories(0,
                this::showLoading,
                this::proceedSuccessDirectories,
                error -> AlertManager.showAlertError(getContext()),
                this::hideLoading);
    }

    private void fetchFavoritesPerforms() {
        repository.performs(0, limit,
                this::showLoading,
                this::proceedSuccessPerforms,
                error -> AlertManager.showAlertError(getContext()),
                this::hideLoading);
    }

    private void fetchOldFavoritesPerforms() {
        repository.performs(adapter.getItemCount(), 20,
                this::showLoading,
                this::proceedSuccessOldPerforms,
                error -> AlertManager.showAlertError(getContext()),
                this::hideLoading);
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void setupUI() {
        rvPerforms.setListener(() -> fetchOldFavoritesPerforms());
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        if (rvPerforms.getChildCount() != 0) return;
        contentView.setVisibility(View.GONE);
    }

    private void hideLoading() {
        loadingView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
    }

    private void proceedSuccessPerforms(PerformsResult result) {
        if (adapter == null) {
            adapter = new PerformFavoritesAdapter(getContext(), result.getPerforms(), new DirectoriesAdapter.OnDirectoryClickListener() {
                @Override
                public void onClick(Directory directory) {
                    repository.setFavoriteFilterId(directory.getId());
                    setSelectedFilter();
                    btnFilter.setText(directory.getName());
                    fetchFavoritesPerforms();
                }

                @Override
                public void onCloseClick(Directory directory) {

                }
            });

            rvPerforms.setAdapter(adapter);
        } else {
            adapter.setData(result.getPerforms());
        }

        tvTotal.setText(String.format(getString(R.string.total), result.getTotal()));
    }

    private void proceedSuccessOldPerforms(PerformsResult result) {
        adapter.appendItems(result.getPerforms());
    }

    private void proceedSuccessDirectories(DirectoriesResult result) {
        btnFilter.setEnabled(true);
        directories = result.getDirectories();
        directories.add(0, new Directory(0, "Все композиции"));

        setSelectedFilter();
        fetchFavoritesPerforms();
    }

    private void setSelectedFilter() {
        for (int i = 0; i < directories.size(); i++) {
            if (directories.get(i).getId() == repository.getFavoriteFilterId()) {
                btnFilter.setText(directories.get(i).getName());
            }
        }
    }
}
