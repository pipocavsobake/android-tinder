package ru.shirykalov.anatoly.classiconline.view.common.adapter.performs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.utils.Utils;
import ru.shirykalov.anatoly.classiconline.view.screens.main.MainActivity;

public class PerformVH extends RecyclerView.ViewHolder {

    @BindView(R.id.tvComposer)
    TextView tvComposer;

    @BindView(R.id.tvPiece)
    TextView tvPiece;

    @BindView(R.id.tvPerformers)
    TextView tvPerformers;

    @BindView(R.id.tvUploadDate)
    TextView tvUploadDate;

    @BindView(R.id.tvCountLikes)
    TextView tvCountLikes;

    @BindView(R.id.tvCountComments)
    TextView tvCountComments;

    @BindView(R.id.btnContinue)
    TextView btnContinue;

    @BindView(R.id.btnPlay)
    ImageButton btnPlay;

    @BindView(R.id.btnBrowser)
    ImageButton btnBrowser;

    PerformAdapter.OnFavoriteClickListener listener;

    PerformVH(View itemView, PerformAdapter.OnFavoriteClickListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
    }

    public void setData(Perform perform, Context ctx) {
        tvComposer.setText(perform.getComposer());
        tvPiece.setText(perform.getPiece());
        tvPerformers.setText(TextUtils.join(", ", perform.getPerformers()));
        tvUploadDate.setText(DateUtils.formatDateTime(ctx, perform.getDate().getTime(), 0));
        tvCountLikes.setText(perform.getLikes() + "");
        tvCountComments.setText(perform.getCommentsCount() + "");
        tvCountLikes.setBackgroundTintList(Utils.isFavoriteColor(ctx, perform.isFavorite()));
        btnContinue.setVisibility(perform.getLastMs() > 0 ? View.VISIBLE : View.GONE);

        btnPlay.setOnClickListener(v -> onPlayClick(perform, false, ctx));
        btnBrowser.setOnClickListener(view -> Utils.openBrowser(ctx, perform));
        tvCountLikes.setOnClickListener(view -> {
            ((MainActivity) ctx).likePerform(perform);
            if (listener != null) listener.onLikeClick(perform);
        });
        btnContinue.setOnClickListener(v -> onPlayClick(perform, true, ctx));
    }

    private void onPlayClick(Perform perform, boolean isContinue, Context ctx) {
        if (perform.isHasAudio()) {
            ((MainActivity) ctx).startPlayer(perform, isContinue);
        } else {
            AlertManager.showMessage(ctx, "Это видео");
        }
    }
}
