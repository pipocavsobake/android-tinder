package ru.shirykalov.anatoly.classiconline.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.shirykalov.anatoly.classiconline.data.model.Comment;

public class CommentsResponse {

    @SerializedName("data")
    private List<Comment> data;

    public List<Comment> getComments() {
        return data;
    }
}
