package ru.shirykalov.anatoly.classiconline.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.view.LayoutInflater;

import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.github.pwittchen.reactivenetwork.library.rx2.ConnectivityPredicate;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class Utils {

    private static int MINUTES_IN_AN_HOUR = 60;
    private static int SECONDS_IN_A_MINUTE = 60;

    public static String toDurationTime(int mills) {
        if (mills == 0) return "00:00";
        final int hours = (mills / 1000) / MINUTES_IN_AN_HOUR / SECONDS_IN_A_MINUTE;
        final int minutes = ((mills / 1000) - hoursToSeconds(hours)) / SECONDS_IN_A_MINUTE;
        final int seconds = (mills / 1000) - (hoursToSeconds(hours) + minutesToSeconds(minutes));
        return hours > 0 ? String.format("%02d:%02d:%02d", hours, minutes, seconds) : String.format("%02d:%02d", minutes, seconds);
    }

    private static int hoursToSeconds(int hours) {
        return hours * MINUTES_IN_AN_HOUR * SECONDS_IN_A_MINUTE;
    }

    private static int minutesToSeconds(int minutes) {
        return minutes * SECONDS_IN_A_MINUTE;
    }

    public static Disposable connectivityListener(Context context, Consumer<Connectivity> callback) {
        return ReactiveNetwork
                .observeNetworkConnectivity(context)
                .subscribeOn(Schedulers.io())
                .filter(ConnectivityPredicate.hasState(NetworkInfo.State.CONNECTED))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback);
    }

    public static Disposable updatesPlayerTime(Consumer<Long> callback) {
        return Observable
                .interval(200, MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .subscribe(callback);
    }

    public static void openBrowser(Context context, Perform perform) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, perform.href()));
    }

    public static ColorStateList isFavoriteColor(Context context, boolean isFavorite) {
        return isFavorite ? ColorStateList.valueOf(Color.RED) : ColorStateList.valueOf(context.getResources().getColor(R.color.grey));
    }
}
