package ru.shirykalov.anatoly.classiconline.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import ru.shirykalov.anatoly.classiconline.R;

import static android.app.NotificationManager.IMPORTANCE_HIGH;

public class NotificationsUtils {

    public static final String CHANNEL_ID = "CLON_CHANNEL";
    private static final int NOTIFICATION_ID = 101;

    public static void showNotification(Service context, NotificationCompat.Builder builder) {
        final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, context.getString(R.string.app_name), IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        } else {
            builder.setVibrate(null);
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }

        context.startForeground(NOTIFICATION_ID, builder.build());
    }

    public static void removeMediaNotification(Context context) {
        final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.deleteNotificationChannel(CHANNEL_ID);
        } else {
            notificationManager.cancelAll();
        }
    }
}
