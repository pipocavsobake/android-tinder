package ru.shirykalov.anatoly.classiconline.player;

enum PlayerAction {
    INIT, PLAY_PAUSE, PLAY, PAUSE, STOP, FAVORITE, BROWSER
}