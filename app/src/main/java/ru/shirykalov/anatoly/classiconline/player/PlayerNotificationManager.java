package ru.shirykalov.anatoly.classiconline.player;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.session.MediaSessionManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;

import ru.shirykalov.anatoly.classiconline.view.screens.main.MainActivity;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.utils.NotificationsUtils;

import static android.content.Context.MEDIA_SESSION_SERVICE;
import static android.support.v4.app.NotificationCompat.PRIORITY_MAX;
import static android.support.v4.media.session.MediaSessionCompat.Callback;
import static android.support.v4.media.session.MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS;
import static ru.shirykalov.anatoly.classiconline.R.drawable.exo_icon_stop;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_browser;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_favorite_default;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_favorite_selected;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_pause;
import static ru.shirykalov.anatoly.classiconline.R.drawable.ic_play;
import static ru.shirykalov.anatoly.classiconline.player.PlaybackStatus.PAUSED;
import static ru.shirykalov.anatoly.classiconline.player.PlaybackStatus.PLAYING;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.BROWSER;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.FAVORITE;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.PAUSE;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.PLAY;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.STOP;
import static ru.shirykalov.anatoly.classiconline.player.PlayerAction.valueOf;
import static ru.shirykalov.anatoly.classiconline.utils.NotificationsUtils.CHANNEL_ID;
import static ru.shirykalov.anatoly.classiconline.utils.NotificationsUtils.removeMediaNotification;

class PlayerNotificationManager {

    private PlayerService service;

    private MediaSessionManager mediaSessionManager;
    private MediaControllerCompat.TransportControls transportControls;
    private MediaSessionCompat mediaSession;

    private Perform activeAudio;

    private PlaybackStatus currentStatus = PLAYING;


    PlayerNotificationManager(PlayerService service) {
        this.service = service;
    }

    void setActiveAudio(Perform activeAudio) {
        this.activeAudio = activeAudio;
    }

    void initMediaSession(Callback mediaSessionCallback) {
        if (mediaSessionManager != null) return;

        mediaSessionManager = (MediaSessionManager) service.getSystemService(MEDIA_SESSION_SERVICE);
        mediaSession = new MediaSessionCompat(service.getApplicationContext(), "AudioPlayer");
        transportControls = mediaSession.getController().getTransportControls();
        mediaSession.setActive(true);
        mediaSession.setFlags(FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setCallback(new Callback() {
            @Override
            public void onPlay() {
                super.onPlay();
                mediaSessionCallback.onPlay();
                buildNotification(PLAYING);
            }

            @Override
            public void onPause() {
                super.onPause();
                mediaSessionCallback.onPause();
                buildNotification(PAUSED);
            }

            @Override
            public void onSkipToNext() {
                super.onSkipToNext();
                mediaSessionCallback.onSkipToNext();
                buildNotification(PLAYING);
            }

            @Override
            public void onSkipToPrevious() {
                super.onSkipToPrevious();
                mediaSessionCallback.onSkipToPrevious();
                buildNotification(PLAYING);
            }

            @Override
            public void onStop() {
                super.onStop();
                mediaSessionCallback.onStop();
                removeNotification();
            }

            @Override
            public void onSeekTo(long position) {
                super.onSeekTo(position);
                mediaSessionCallback.onSeekTo(position);
            }

            @Override
            public void onCustomAction(String action, Bundle extras) {
                super.onCustomAction(action, extras);
                mediaSessionCallback.onCustomAction(action, extras);
            }
        });
    }

    void handleIncomingActions(Intent playbackAction) {
        if (playbackAction == null || playbackAction.getAction() == null) return;

        PlayerAction action = valueOf(playbackAction.getAction());

        switch (action) {
            case PLAY:
                transportControls.play();
                break;
            case PAUSE:
                transportControls.pause();
                break;
            case STOP:
                transportControls.stop();
                break;
            default:
                transportControls.sendCustomAction(action.name(), new Bundle());
                break;
        }
    }

    void seekTo() {
        buildNotification(currentStatus);
    }

    void buildNotification(PlaybackStatus playbackStatus) {
        currentStatus = playbackStatus;

        int notificationAction = ic_pause;
        PendingIntent play_pauseAction = null;

        switch (playbackStatus) {
            case PLAYING:
                notificationAction = ic_pause;
                play_pauseAction = playbackAction(PAUSE);
                break;
            case PAUSED:
                notificationAction = ic_play;
                play_pauseAction = playbackAction(PLAY);
                break;
        }

        boolean isPlaying = playbackStatus == PLAYING;

        Intent notifyIntent = new Intent(service, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        PendingIntent intentActivity = PendingIntent.getActivity(service, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(service, CHANNEL_ID)
                .setShowWhen(false)
                .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle()
                        .setMediaSession(mediaSession.getSessionToken())
                        .setShowActionsInCompactView(1, 2, 3))
                .setSmallIcon(android.R.drawable.stat_sys_headset)
                .setContentText(activeAudio.getNotificationTitle())
                .setContentTitle(activeAudio.getPiece())
                .setContentIntent(intentActivity)
                .setPriority(PRIORITY_MAX)
                .setWhen(isPlaying ? System.currentTimeMillis() - service.getResumePosition() : 0)
                .setShowWhen(isPlaying)
                .setUsesChronometer(isPlaying)
                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setOnlyAlertOnce(true)
                .addAction(ic_browser, "browse", playbackAction(BROWSER))
                .addAction(notificationAction, "pause", play_pauseAction)
                .addAction(activeAudio.isFavorite() ? ic_favorite_selected : ic_favorite_default, "favorite", playbackAction(FAVORITE))
                .addAction(exo_icon_stop, "stop", playbackAction(STOP));

        NotificationsUtils.showNotification(service, builder);
    }

    void release() {
        if (mediaSession != null) mediaSession.release();
        removeNotification();
    }

    void setFavorite() {
        activeAudio.setFavorite(true);
        buildNotification(getStatus());
    }

    PlaybackStatus getStatus() {
        return currentStatus;
    }

    private PendingIntent playbackAction(PlayerAction action) {
        Intent intent = new Intent(service, PlayerService.class);
        intent.setAction(action.name());
        return PendingIntent.getService(service, action.ordinal(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void removeNotification() {
        removeMediaNotification(service);
    }
}
