package ru.shirykalov.anatoly.classiconline.player;

public enum PlaybackStatus {
    INIT, PLAYING, PAUSED
}
