package ru.shirykalov.anatoly.classiconline.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SystemUtils {

    public static SharedPreferences getPreferences(Context ctx) {
        return ctx.getSharedPreferences("preferences", Context.MODE_PRIVATE);
    }
}
