package ru.shirykalov.anatoly.classiconline.data.api;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.shirykalov.anatoly.classiconline.data.model.Comment;
import ru.shirykalov.anatoly.classiconline.data.model.Directory;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.data.model.request.CommentRequest;
import ru.shirykalov.anatoly.classiconline.data.model.request.CreateDirectoryRequest;
import ru.shirykalov.anatoly.classiconline.data.model.request.EventRequest;
import ru.shirykalov.anatoly.classiconline.data.model.request.LoginRequest;
import ru.shirykalov.anatoly.classiconline.data.model.response.CommentsResponse;
import ru.shirykalov.anatoly.classiconline.data.model.response.LoginResponse;
import ru.shirykalov.anatoly.classiconline.data.model.result.DirectoriesResult;
import ru.shirykalov.anatoly.classiconline.data.model.result.PerformsResult;

public interface ClonAPI {

    @POST("auth/password/login")
    @Headers({"Accept: application/json", "Content-Type: Application/json"})
    Observable<LoginResponse> login(@Body LoginRequest request);

    @PUT("api/performs/{performId}/like")
    Observable<ResponseBody> like(
            @Path("performId") int performId,
            @HeaderMap Map<String, String> headers);

    @GET("api/perform/{performId}")
    Observable<Perform> perform(
            @Path("performId") int performId,
            @HeaderMap Map<String, String> headers);

    @GET("api/performs")
    Observable<PerformsResult> recentPerforms(
            @Query("offset") int offset,
            @Query("limit") int limit,
            @Query("likes_gte") int likes_gte,
            @Query("comments_count_gte") int comments_count_gte,
            @Query("order") String order,
            @Query("desc") boolean desc,
            @Query("has_audio") boolean has_audio,
            @HeaderMap Map<String, String> headers);

    @GET("search")
    Observable<PerformsResult> searchPerforms(
            @Query(encoded = true, value = "q") String query,
            @Query("offset") int offset,
            @Query("limit") int limit,
            @HeaderMap Map<String, String> headers);

    @GET("api/performs/favorite")
    Observable<PerformsResult> favorites(
            @Query("offset") int page,
            @Query("limit") int limit,
            @HeaderMap Map<String, String> headers);

    @GET("api/directories/{directoryId}/performs")
    Observable<PerformsResult> performsDirectory(
            @Path("directoryId") int directoryId,
            @Query("offset") int offset,
            @Query("offset") int limit,
            @HeaderMap Map<String, String> headers);

    @GET("api/directories")
    Observable<DirectoriesResult> directories(
            @Query("limit") int limit,
            @Query("offset") int offset,
            @HeaderMap Map<String, String> headers);

    @POST("api/directories")
    Observable<Response<Directory>> createDirectory(
            @Body CreateDirectoryRequest request,
            @HeaderMap Map<String, String> headers);

    @POST("api/directory/{directoryId}/performs/{performId}")
    Observable<Response<Void>> addPerformToDirectory(
            @Path("directoryId") int directoryId,
            @Path("performId") int performId,
            @HeaderMap Map<String, String> headers);

    @DELETE("/api/directory/{directoryId}/performs/{performId}")
    Observable<Response<Void>> removePerformDirectory(
            @Path("directoryId") int directoryId,
            @Path("performId") int performId,
            @HeaderMap Map<String, String> headers);

    @POST("api/events")
    Observable<Response<Void>> sentEvent(
            @Body EventRequest eventRequest,
            @HeaderMap Map<String, String> headers);

    @GET("api/comments/")
    Observable<Response<CommentsResponse>> comments(
            @HeaderMap Map<String, String> headers);

    @PUT("api/comments/")
    Observable<Response<Void>> sentComments(
            @Body CommentRequest request,
            @HeaderMap Map<String, String> headers);

}
