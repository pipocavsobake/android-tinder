package ru.shirykalov.anatoly.classiconline.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Directory;
import ru.shirykalov.anatoly.classiconline.utils.interfaces.ChangeHostListener;

/**
 * Created by Elyseev Vladimir on 26.08.2018.
 */
public class AlertManager {

    public static void showMessage(Context ctx, String message) {
        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
    }

    public static void showAlertMessage(Context ctx, String message) {
        new AlertDialog.Builder(ctx)
                .setMessage(message)
                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    public static void showFilter(Context ctx, List<Directory> directories, int favoriteFilterId, DialogInterface.OnClickListener listener) {
        final List<String> names = new ArrayList<>();
        int checkedPosition = 0;

        for (int i = 0; i < directories.size(); i++) {
            if (directories.get(i).getId() == favoriteFilterId) {
                checkedPosition = i;
            }
            names.add(directories.get(i).getName());
        }

        final ArrayAdapter listAdapter = new ArrayAdapter<>(ctx, android.R.layout.simple_list_item_single_choice, names);

        new AlertDialog.Builder(ctx)
                .setTitle("Фильтр")
                .setSingleChoiceItems(listAdapter, checkedPosition, null)
                .setPositiveButton("ОК", (dialog, whichButton) -> listener.onClick(dialog, whichButton))
                .setNegativeButton("Отмена", (dialog, which) -> dialog.dismiss())
                .show();
    }

    public static void showAlertError(Context ctx) {
        showAlertMessage(ctx, "Произошла ошибка");
    }

    public static void showAlertError(Context context, Throwable error) {
        showAlertMessage(context, String.format(context.getString(R.string.error_body), error.getMessage()));

    }

    public static void showAlertEditHost(Context ctx, String currentHost, ChangeHostListener listener) {
        final View view = ((LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_alert_edit_host, null, false);
        final EditText etHost = view.findViewById(R.id.etHost);
        etHost.setText(currentHost);

        new AlertDialog.Builder(ctx)
                .setTitle("Изменить host")
                .setView(view)
                .setPositiveButton("OK", (dialog, which) -> listener.onChangeHost(etHost.getText().toString()))
                .setNeutralButton("По умолчанию", (dialog, which) -> listener.onChangeHost("clonclient.shirykalov.ru"))
                .setNegativeButton("Отмена", (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }
}
