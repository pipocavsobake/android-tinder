package ru.shirykalov.anatoly.classiconline.data.model.result;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.shirykalov.anatoly.classiconline.data.model.Perform;

public class PerformsResult {

    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("data")
    @Expose
    private List<Perform> performs;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Perform> getPerforms() {
        return performs;
    }

    public void setData(List<Perform> performs) {
        this.performs = performs;
    }
}
