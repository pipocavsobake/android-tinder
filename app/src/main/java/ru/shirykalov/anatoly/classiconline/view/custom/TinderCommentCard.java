package ru.shirykalov.anatoly.classiconline.view.custom;

import android.widget.TextView;

import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;

import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Comment;
import ru.shirykalov.anatoly.classiconline.view.screens.comments.CommentsListener;

@Layout(R.layout.view_tinder_comment)
public class TinderCommentCard {

    @View(R.id.tvComment)
    private TextView tvComment;

    private Comment comment;
    private CommentsListener commentListener;

    public TinderCommentCard(Comment comment, CommentsListener commentListener) {
        this.comment = comment;
        this.commentListener = commentListener;
    }

    @Resolve
    private void onResolved() {
        tvComment.setText(comment.getText());
    }

    @SwipeOut
    private void onSwipedDeclined() {
        comment.setDeclined(true);
        commentListener.onCommentDeclined(comment);
    }

    @SwipeIn
    private void onSwipedApproved() {
        comment.setApproved(true);
        commentListener.onCommentApproved(comment);
    }
}
