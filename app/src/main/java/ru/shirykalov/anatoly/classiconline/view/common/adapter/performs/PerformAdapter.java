package ru.shirykalov.anatoly.classiconline.view.common.adapter.performs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;

public class PerformAdapter extends RecyclerView.Adapter<PerformVH> {

    private OnFavoriteClickListener favoriteClickListener;
    private List<Perform> performs = new ArrayList<>();
    private Context context;

    public PerformAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public PerformVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PerformVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_perform, parent, false), favoriteClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final PerformVH holder, final int position) {
        holder.setData(performs.get(position), context);
    }

    @Override
    public int getItemCount() {
        return performs.size();
    }

    public void setFavoriteClickListener(OnFavoriteClickListener favoriteClickListener) {
        this.favoriteClickListener = favoriteClickListener;
    }

    public void setItems(List<Perform> performs) {
        this.performs = performs;
        notifyDataSetChanged();
    }

    public void appendItems(List<Perform> data) {
        int index = getItemCount();
        performs.addAll(data);
        notifyItemRangeInserted(index + 1, data.size());
    }

    public void setLike(Perform perform) {
        final int position = performs.indexOf(perform);
        performs.get(position).setFavorite(true);
        notifyItemChanged(position);
    }

    public interface OnFavoriteClickListener {
        void onLikeClick(Perform perform);
    }
}
