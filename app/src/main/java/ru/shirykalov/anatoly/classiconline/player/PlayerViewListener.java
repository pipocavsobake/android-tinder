package ru.shirykalov.anatoly.classiconline.player;

import ru.shirykalov.anatoly.classiconline.data.model.Perform;

public interface PlayerViewListener {

    void onPlayPauseClick();

    void onFavoriteClick(Perform perform);

    void startSeek();

    void endSeek(int progress);

    void onCloseFullscreenPlayer();

    void onStopClick();
}