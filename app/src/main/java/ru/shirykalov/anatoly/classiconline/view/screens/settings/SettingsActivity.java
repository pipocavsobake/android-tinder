package ru.shirykalov.anatoly.classiconline.view.screens.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import ru.shirykalov.anatoly.classiconline.App;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.data.repository.statistic.StatisticRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.statistic.StatisticRepositoryImpl;
import ru.shirykalov.anatoly.classiconline.player.PlayerService;
import ru.shirykalov.anatoly.classiconline.utils.AlertManager;
import ru.shirykalov.anatoly.classiconline.view.screens.directories.DirectoriesActivity;
import ru.shirykalov.anatoly.classiconline.view.screens.login.LoginActivity;
import ru.shirykalov.anatoly.classiconline.view.screens.main.MainActivity;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.cbStatistics)
    CheckBox checkStatistics;

    @BindView(R.id.tvHost)
    TextView tvHost;

    private StatisticRepository statisticRepository;
    private AuthRepository authRepository;


    //==============================================================================================
    // Static
    //==============================================================================================

    public static void start(AppCompatActivity ctx) {
        ctx.startActivityForResult(new Intent(ctx, SettingsActivity.class), 100);
    }


    //==============================================================================================
    // Android
    //==============================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        statisticRepository = new StatisticRepositoryImpl(this);
        authRepository = new AuthRepositoryImpl(this);
        setupToolbar();
        setupUI();
    }

    @OnClick(R.id.viewStatistics)
    public void onStatisticsClick(View v) {
        statisticRepository.setCheckStatistics(!statisticRepository.getCheckStatistics());
        checkStatistics.setChecked(statisticRepository.getCheckStatistics());
    }

    @OnClick(R.id.viewHost)
    public void onHostClick(View v) {
        AlertManager.showAlertEditHost(this, authRepository.getBaseHost(), this::proceedChangeHost);
    }

    @OnCheckedChanged(R.id.cbStatistics)
    public void onStatisticsCheckedChange(boolean isChecked) {
        statisticRepository.setCheckStatistics(isChecked);
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void setupToolbar() {
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void setupUI() {
        tvHost.setText(authRepository.getBaseHost());
        checkStatistics.setChecked(statisticRepository.getCheckStatistics());
    }

    private void proceedChangeHost(String host) {
        stopService(new Intent(this, PlayerService.class));
        authRepository.setBaseHost(host);
        App.getInstance().initApi();
        setResult(RESULT_OK);
        finish();
    }
}
