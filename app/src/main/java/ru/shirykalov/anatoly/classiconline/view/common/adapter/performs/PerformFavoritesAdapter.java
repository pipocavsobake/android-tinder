package ru.shirykalov.anatoly.classiconline.view.common.adapter.performs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Perform;
import ru.shirykalov.anatoly.classiconline.view.screens.favorites.PerformDiffUtilCallback;
import ru.shirykalov.anatoly.classiconline.view.screens.favorites.adapter.directories.DirectoriesAdapter;

public class PerformFavoritesAdapter extends RecyclerView.Adapter<PerformFavoriteVH> {

    private DirectoriesAdapter.OnDirectoryClickListener listener;
    private List<Perform> performs;
    private Context context;

    public PerformFavoritesAdapter(Context context, List<Perform> performs, DirectoriesAdapter.OnDirectoryClickListener listener) {
        this.performs = performs;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public PerformFavoriteVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PerformFavoriteVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite_perform, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(@NonNull final PerformFavoriteVH holder, final int position) {
        holder.setData(performs.get(position), context);
    }

    @Override
    public int getItemCount() {
        return performs.size();
    }

    public void setData(List<Perform> newPerforms) {
        final DiffUtil.DiffResult productDiffResult = DiffUtil.calculateDiff(new PerformDiffUtilCallback(performs, newPerforms));
        performs = newPerforms;
        productDiffResult.dispatchUpdatesTo(this);
    }

    public void appendItems(List<Perform> data) {
        int index = getItemCount();
        performs.addAll(data);
        notifyItemRangeInserted(index + 1, data.size());
    }
}
