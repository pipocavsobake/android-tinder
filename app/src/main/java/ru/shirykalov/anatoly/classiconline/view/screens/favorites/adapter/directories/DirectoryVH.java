package ru.shirykalov.anatoly.classiconline.view.screens.favorites.adapter.directories;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.model.Directory;

public class DirectoryVH extends RecyclerView.ViewHolder {

    @BindView(R.id.tvDirectory)
    TextView tvDirectory;

    private DirectoriesAdapter.OnDirectoryClickListener listener;

    DirectoryVH(View itemView, DirectoriesAdapter.OnDirectoryClickListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
    }

    public void setDirectory(Directory directory) {
        tvDirectory.setText(directory.getName());
        itemView.setOnClickListener(v -> listener.onClick(directory));
    }
}
