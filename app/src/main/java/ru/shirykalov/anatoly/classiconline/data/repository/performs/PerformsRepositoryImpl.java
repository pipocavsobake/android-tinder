package ru.shirykalov.anatoly.classiconline.data.repository.performs;

import android.content.Context;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.shirykalov.anatoly.classiconline.App;
import ru.shirykalov.anatoly.classiconline.data.model.response.Response;
import ru.shirykalov.anatoly.classiconline.data.model.result.PerformsResult;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;

public class PerformsRepositoryImpl implements PerformsRepository {

    private AuthRepository authRepository;

    public PerformsRepositoryImpl(Context ctx) {
        this.authRepository = new AuthRepositoryImpl(ctx);
    }

    @Override
    public void recentPerforms(
            int offset,
            int comments,
            int likes,
            boolean isAudio,
            Response.Start startListener,
            Response.Success<PerformsResult> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().recentPerforms(offset, 20, likes, comments, "upload_date", true, isAudio, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> startListener.onStart())
                .doFinally(completeListener::onComplete)
                .subscribe(successListener::onSuccess, errorListener::onError);
    }

    @Override
    public void searchPerforms(
            String query,
            int offset,
            Response.Start startListener,
            Response.Success<PerformsResult> successListener,
            Response.Error errorListener,
            Response.Complete completeListener
    ) {
        App.getApi().searchPerforms(query, offset, 20, authRepository.getHeaders())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> startListener.onStart())
                .doFinally(completeListener::onComplete)
                .subscribe(successListener::onSuccess, errorListener::onError);
    }
}
