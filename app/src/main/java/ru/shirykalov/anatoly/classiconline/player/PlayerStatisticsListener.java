package ru.shirykalov.anatoly.classiconline.player;

public interface PlayerStatisticsListener {

    void onError();

    void onCompletion();
}
