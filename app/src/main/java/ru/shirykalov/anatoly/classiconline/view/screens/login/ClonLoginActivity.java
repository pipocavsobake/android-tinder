package ru.shirykalov.anatoly.classiconline.view.screens.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.shirykalov.anatoly.classiconline.R;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepository;
import ru.shirykalov.anatoly.classiconline.data.repository.login.AuthRepositoryImpl;

public class ClonLoginActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.wvLogin)
    WebView wvLogin;

    private AuthRepository authRepository;

    private volatile Map<String, String> headers;


    //==============================================================================================
    // Static
    //==============================================================================================

    public static void start(Context ctx) {
        ctx.startActivity(new Intent(ctx, ClonLoginActivity.class));
    }


    //==============================================================================================
    // Android
    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clon_login);
        authRepository = new AuthRepositoryImpl(this);
        ButterKnife.bind(this);
        setupToolbar();
        setupUI();
    }


    //==============================================================================================
    // Private
    //==============================================================================================

    private void setupToolbar() {
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void setupUI() {
        wvLogin.getSettings().setJavaScriptEnabled(true);
        wvLogin.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, final String url) {
                super.onPageFinished(view, url);
                view.evaluateJavascript("(document.querySelector('a[href=\"/account/profile/\"]') != null)", value -> {
                    if (!value.equals("true")) return;

                    headers.put("Cookie", CookieManager.getInstance().getCookie(url));
                    authRepository.saveClonHeaders(headers);
                    finish();
                });
            }

            @SuppressLint("NewApi")
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                headers = request.getRequestHeaders();
                return null;
            }
        });

        wvLogin.loadUrl("http://classic-online.ru");
    }
}
